
.cseg        

.def io_set =  r16					; used to set output on port D
.def workhorse = r17				; used to move values 
.def counter = r18
.org 0x0000							; reset vector
	rjmp setup
.org 0x0020							; begin at location 20
	rjmp ISR_OVF0
.org 0x0100          


setup:	ser io_set					; sets all bits to 1 in io_set
		out	DDRD, io_set			; use io_set to set all pins on PORTD to output
		ldi workhorse, 0b00000000	; Timer0 normal mode 
		out TCCR0A,workhorse		
		ldi workhorse, 0b00000101	; load prescalar
		out TCCR0B, workhorse
		ldi workhorse, 0b00000001	; load amount of steps into workhorse
		sts TIMSK0, workhorse		; load value of workhorse into timer
		ldi counter, 0				; setting up value for variable counter
		sei							; set gloval interrupt flag
		
loop:	out PORTD, counter			; output value of counter to PORTD
		rjmp loop

ISR_OVF0: inc counter				; increment value of counter
		  reti						; return from interrupt
		/**push  workhorse
		in    workhorse, SREG
		push  workhorse
		sbi   PIND, 0
		pop   workhorse
		out   SREG, workhorse
		pop   workhorse
		reti
	  */
